/*
 * ======== Standard MSP430 includes ========
 */
#include <msp430.h>

/*
 * ======== Grace related includes ========
 */
#include <ti/mcu/msp430/csl/CSL.h>


//Define operators improve code legibility, BIT0-BIT7 had previously been defined by the include
#define HIGH |=
#define LOW &=~
//

//control functions
void RPM();
void PWM();
void killAll();
void pollTemperature(char motor);
void lights(char lightsOn);
unsigned char processCommand(unsigned short command);
//UART output functions
void intBlueTooth();
void sendCommand(char *string);
void sendByte(char byte);
void sendShort(short data);
//UART input functions
__interrupt void RX_ISR();
void decrypt(unsigned short data);
//Global command variables
/*
These variables control the input stream of commands, they are written to only by the interupt
 */
unsigned short seed = 3000;
unsigned short heartbeat = -1;//Unsigned_short max ~= 65k = 0.4 seconds of on time, this is saved here for benchmarking purposes
char validCommand = 1;

void main(void)
{
	CSL_init();//Grace-generated pin configurations
	P1OUT HIGH BIT0;//Red status light on
	intBlueTooth();//Initialize the blue tooth
	lights(1);
	while(1)//This loop will execute about 100,000 times a second
	{
		RPM();
		if(heartbeat > 0)//Normal operation
		{
			heartbeat--;
			PWM();
		}
		else//Terminates the motors, lights, and sends the seed value so that it can be synchronized with the phone
		{
			killAll(validCommand);
			__delay_cycles(1000000);
			sendShort(seed);
			P1OUT ^= BIT0;//Just a simple blinking LED to indicate it's disconnected
		}
	}
}

//Sends the result of a ping whenever a wheel has completed a rotation
/*
This sensor system works using hall effect sensors wired to a comparator op amp.
the voltatage output is either lightly higher or lower than average, by about 2mV
When it's high, 1 pin is high while the other is low, when low, the other pin is high and the original pin is low, otherwise the state can be 11 or 00
 */
//Global RPM state variable
/*
This is controlled by the RPM system
 */
//1 and 4 indicate the wheel triggers
//2 and 3 indicate pedal triggers
char lastState = 0;
char setBits = 0;
void RPM()
{
	char currentState = P2IN & 0x1E;
	if((currentState ^ lastState) == 0)//If The pins have been the same, do nothing
	{
		return;
	}
	short output = 0;
	//Test the wheel pins of 1 and 4
	char wheelPins = currentState & 0x12;
	if(wheelPins == 0x02)//Calls the set
	{
		setBits HIGH 0x01;
	}
	else if(wheelPins == 0x10)
	{
		if(setBits & 0x01)//If the set was called and the next state is high, reset and send
		{
			setBits LOW 0x01;
			output HIGH BIT0;
		}
	}
	//Tests the pedal pins of 2 and 3
	char pedalPins = currentState & 0x0C;
	if(pedalPins == 0x04)
	{
		setBits HIGH 0x02;
	}
	else if(pedalPins == 0x08)
	{
		if(setBits & 0x02)
		{
			setBits LOW 0x02;
			output HIGH BIT1;
		}
	}
	if(output != 0)//If the output has been set by any of these functions
	{
		sendShort(output);
	}
	lastState = currentState;
}

//This controls the motor's PWM duty cycle
//Global PWM cycle variables
/*
onTime and totalTime are written to by processCommand() and read by PWM()
 */
unsigned short onTime = 0;
unsigned short totalTime = 0;
unsigned short count = 0;

unsigned char processCommand(unsigned short command)
{
	P1OUT HIGH BIT0;
	unsigned short instructionClass = command & 0x0003;
	unsigned short instruction = command >> 2;
	switch (instructionClass)
	{
	case 0://Invalid
		return 0;
	case 1://Sensor and heart beat
		switch (instruction)
		{
		case 0:
			pollTemperature(0);//Driver temperature
			return 1;
		case 1:
			pollTemperature(1);//Motor temperature
			return 1;
		case 2://Lights off
			lights(0);
			return 1;
		case 3://Lights on
			lights(1);
			return 1;
		}
		return 0;
	case 2://totalTime is initialized first
		onTime = 0;
		totalTime = instruction;
		return 1;
	case 3://onTime also performs checks for validity
		onTime = instruction;
		if (onTime > totalTime)
		{
			onTime = 0;
			totalTime = 0;
			return 0;
		}
		return 1;
	}
	return 0;
}

void PWM()
{
	count++;
	if(count < onTime)
	{
		P1OUT HIGH BIT6;
	}
	else
	{
		P1OUT LOW BIT6;
		if(count > totalTime)
		{
			count = 0;
		}
	}
}

void killAll()
{
	P1OUT LOW BIT6;
	onTime = 0;
	totalTime = 0;
	lights(0);
	if(validCommand == 0)//A 10 second cool down timer activates if the system entered an invalid command
	{
		sendShort(2500);
		//This is a 10 second delay timer, during this time it flashes SOS in morse code
		//This serves as a diagnostic to identify the cause of the problem if it doesn't start
		//And if anyone is trying to break into it, this should be amusing enough for them to leave it alone
		//I call this 'Security by Distraction'
		unsigned short i = 0;//apparently for loops are not supported either
		while(i < 100)
		{
			short j = i%20;
			if(j < 9)
			{
				if(j < 3 || j > 5)//Dot
				{
					lights(1);
					P1OUT HIGH BIT0;
					__delay_cycles(100000);
					lights(0);
					P1OUT LOW BIT0;
					}
				else//Dash
				{
					__delay_cycles(1000000);
					lights(1);
					P1OUT HIGH BIT0;
					__delay_cycles(2000000);
					lights(0);
					P1OUT LOW BIT0;
					__delay_cycles(1000000);
				}
			}
			__delay_cycles(1000000);
			i++;
			sendShort(seed);
		}
		validCommand = 1;
	}
}

//This polls the sensor and sends the data over the line\
//The ADC is 10 bits and I insert an extra lowest bit to indicate the source
void pollTemperature(char motor)
{
	P1OUT LOW BIT6;//Safety call to disable system before while loop is called
	short pin = 5;//Driver is pin 5
	if(motor == 1)//Motor is pin 4
	{
		pin = 4;
	}
	ADC10CTL1 = pin << 12;//Select the pin to be read
	ADC10CTL0 HIGH ADC10SC;
	while(ADC10CTL1 & ADC10BUSY);
	unsigned short data = ADC10MEM;
	if(data < 3)//Values less than 3 fall would be misinterpreted as a RPM ping when you factor in the bitshift later, this stops that
	{
		data = 3;
	}
	data = data << 1;
	if(motor == 1)//Motor temp
	{
		data++;//Adds a 1 to the end of this value indicating the type
	}
	sendShort(data);
}

void lights(char lightsOn)
{
	if(lightsOn)//On
	{
		P1OUT HIGH BIT7;
	}
	else
	{
		P1OUT LOW BIT7;
	}
}

//This initiates the UART settings and registers along with configures bridge
//Tested and working
void intBlueTooth()
{
	//Enable UART support
	UCA0CTL0 = 0;
	UCA0CTL1 = UCSSEL_2;
	UCA0MCTL = UCBRS0;
	//Creates the RX input interrupt
	IE2 = UCA0RXIE;
	// Set DCO to 1MHz because of UCA0BR0 limitation
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL = CALDCO_1MHZ;
	//sets the number of clock cycles for each bit, 1MHz/9600 bits = 104
	UCA0BR0 = 104;
	UCA0BR1 = 0;
	//There is a bug in the module where it ignores the first character received
	if(0){sendCommand("A\0");
	//The module should save the last settings, but if it returns to default, this will reset it
	sendCommand("AT+NAMEEVnaught\0");	//Sets the name
	sendCommand("AT+PIN2358\0");		//Sets the access pin
	sendCommand("AT+BAUD8\0");}			//Sets baud to 115200
	// Set DCO back to 8MHz and sets the final baud rate to 8MHz/115200
	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;
	UCA0BR0 = 69;//sets the number of clock cycles for each bit, 8MHz/115200 bits = 69
	return;
}

//Sends a string to TX
//Tested and working
void sendCommand(char *string)
{
	short i = 3;//3 second wait between commands is added both to he front and back
	if(BCSCTL1 != CALBC1_1MHZ)//Assume worst case if clock speed is not 1MHz
	{
		i = i*8;
	}
	short j = 0;
	while(j < i)
	{
		j++;
		__delay_cycles(1000000);
	}
	while(*string != 0)
	{
		sendByte(*string);
		string++;
	}
	j = 0;
	while(j < i)
	{
		j++;
		__delay_cycles(1000000);
	}
}

//Sends a char to TX
//Tested and working
void sendByte(char byte)
{
	P1OUT LOW BIT6;//Safety system before while loop is called
	UCA0TXBUF = byte;
	while(UCA0STAT != 0);		//Flag is reset when computation is complete
	__delay_cycles(50);			//Adds separator bits between bytes
	return;
}

//Sends a unsigned short to the TX interface.
//Tested and working
void sendShort(short data)//Maximum data size is actually unsigned 14 bits
{
	char second = 0x7F & data;
	second += 0x80;
	data = data >> 7;
	char first = 0x7F & data;
	sendByte(first);
	sendByte(second);
}

//Receive character from RX
//Tested and working
unsigned short tempCommand = 0;
#pragma vector=USCIAB0RX_VECTOR
__interrupt void RX_ISR()
{
	short data = UCA0RXBUF;
	if(data < 128)//first and highest byte starts with 0
	{
		tempCommand = (data & 0x7F) << 7;
	}
	else//second byte starts with a 1, starts the decrypt
	{
		data = data & 0x7F;
		tempCommand += data;
		decrypt(tempCommand);
		tempCommand = 0;
	}
}

//Decrypts the received command, this is called automatically, sent commands are not encrypted
//Tested and working
void decrypt(unsigned short data)//ALL CODE IN THIS FUNCTION NEEDS TO BE DUPLICATED ON BOTH MICROPROCESSOR AND ANDROID
{
	if(validCommand)//Verifies that the system is not shutdown due to broken command
	{
		seed++;			//Increments the seed for the next computation
		if(seed > 16000)
		{
			seed = 3000;
		}
		validCommand = processCommand(data);
		if(validCommand)//If the latest command is valid, reset the heartbeat counter
		{
			heartbeat = -1;
		}
	}
	//Computes the key using crude cipher
	short key = seed;
	short temp = (short) (key & 0x0FF0);
	temp = (short) (temp >> 4);
	temp = (short) (temp * temp);
	key *= (key-temp);
	key = (short)~key;
	key += seed;
	key *= (key-seed);
	if(seed % 5 == 0)
		{
		temp = (short) (key << 8);
		temp = (short) ~temp;
		key = (short) (key >> 8);
		key = (short) (key + temp);
	}
	if(seed % 7 == 0)
	{
		temp = (short)(seed % 0x00FF);
		temp = (short)(temp << 8);
		key -= temp;
	}
	if(seed % 3 == 0)
	{
		key = (short)(key & 0xFFFC);
	}
	data ^= (key & 0x3FFF);	//Insures the generated key is in range
	return data;
}